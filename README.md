# Mini-projet 1 Codage Hauffman

## Installation
Afin de pouvoir accéder à ce code, il faut que vous cloniez l'ensemble des fichiers sur votre ordinateur.
Pour ce faire : git clone + le chemin d'accès fournit par gitlab

## Usage
A l'aide de VisualStudio ou d'Eclipse (ou équivalent), ouvrez le dossier. Dedans source, vous trouverez une classe nommez Test.java. Ouvrez-la.
Afin de pouvoir tester les fonctionnalités que vous souhaitez, dé-commentez les lignes correspondates et observez ce qui est afficher.
