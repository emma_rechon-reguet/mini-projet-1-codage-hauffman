//import java.io.FileReader;
/*Librairie permettant de lire les fichier au format txt. A faire à la fin */
import java.util.*;
/*Import de toutes les classes de java.util : ArrayList, LinkedHashedSet */
import java.io.File;

public class Analyse_doc {
    /*texte à analyser*/
    private String txt ;
    /*tableau contenant l'ensemble des caractères non triés */
    private ArrayList <String> cNT;
    /*tableau contenant l'ensemble des caractères triés : il enlève les doublons de manière automatique */
    private LinkedHashSet <String> cT;
    /*Dictionnaire contenant l'ensemble des répétitions en fonction d'un caractère*/
    private LinkedHashMap<String, Integer> occurence;
    /* Cas d'un fichier envoyer au format txt */
    File f;

    public Analyse_doc(String txt){
        this.txt=txt;
        cNT=new ArrayList <String> ();
        creerTableau();
        cT=new LinkedHashSet <String> (cNT);
        occurence = new LinkedHashMap<String, Integer>();
    }

    
    private void creerTableau() {
        /*Remplace la chaîne de caractère par un tableau */
        for(int i=0;i<this.txt.length();i++){
            /*La méthode substring permet de décomposer une chaîne de caractères en fonction d'un indice de début et de fin */
            cNT.add(txt.substring(i,i+1));
        }
    }

    public LinkedHashSet<String> trierTableau(){
        return cT;
    }

    public String getTxt(){
        return txt;
    }

    public ArrayList<String> getNonTrie(){
        return cNT;
    }

    /*Afficher la liste des éléments triés */
    public LinkedHashSet<String> printTrie(){
        return cT;
    }

    public int getRepetition (String e){
        /*Méthode permettant d'obtenir les répétitions d'un éléments donné en paramètre. 
         * Le nombre de répétition est basé sur le texte de base est non le trié.
         */
        int rep=0;
        if(cT.contains(e)){
            for(int j=0;j<cNT.size();j++){
                /* Puisque que l'on compare des String il faut que l'on utilise .equals et non ==*/
                if(cNT.get(j).equals(e)){
                    rep+=1;
                }
            }
        }
        return rep;
    }

    public LinkedHashMap<String, Integer> getAllOccurence(){
        /*Cette méthode permet d'obtenir les occurences de tous les éléments du tableau. */
        /*On change la liste des éléments triés en un tableau */
        String [] temp_array = cT.toArray(new String[0]);
        for (int i=0;i<temp_array.length;i++){
            occurence.put(temp_array[i],getRepetition(temp_array[i]));
        }
        return occurence;
    }

    
    public LinkedHashMap<String, Integer> trieOccurence(){
        getAllOccurence();
        //création d'un objet à retourner vide
        LinkedHashMap<String,Integer> mapTriee = new LinkedHashMap<String,Integer>();
        //Nous devons prendre un linkedhashmap pour que les éléments enregistrées 

        //on prend la valeurs max possible dans les valeurs
        int max = Collections.max(occurence.values());

        for(int i=1;i<=max;i++){
            for (Map.Entry<String, Integer> entry : occurence.entrySet()){
                if(entry.getValue().equals(i)){
                    mapTriee.put(entry.getKey(),entry.getValue());
                }
            }
        }
        occurence=mapTriee;
        return mapTriee; 
    }

    public int getMaxNodes(){
        return occurence.size();
    }
}
