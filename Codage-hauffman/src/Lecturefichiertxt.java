import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Lecturefichiertxt {
    private Analyse_doc ana;

    public Lecturefichiertxt (String accès , String nom){
        String uri = "Codage-hauffman\\" + accès;
        uri+= "\\"+nom;
        File f=new File(uri);
        try{
            Scanner txt = new Scanner(f);
            while (txt.hasNextLine()){
                ana=new Analyse_doc(txt.nextLine());
            }           
        }catch (IOException e) {
            // Gestion des exceptions en cas d'erreur de lecture du fichier
            e.printStackTrace();
        }
    }

    public Analyse_doc getAnalyse(){
        return ana;
    }
}
