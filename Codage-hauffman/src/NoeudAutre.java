public class NoeudAutre extends Noeud{
    
    private Noeud leftC;
    private Noeud rightC;

    public NoeudAutre(Noeud leftC,Noeud rightC)
    {
        this.leftC=leftC;
        this.rightC=rightC;
        leftC.parent=this;
        rightC.parent=this;
    }

    @Override
    public String getlabel() {
        return null;
    }

    @Override
    public int getPoids() {
        return leftC.getPoids()+rightC.getPoids();
    }

    @Override
    public Noeud getLeftC() {
        return leftC;
    }

    @Override
    public Noeud getRightC() {
        return rightC;
    }

    public String toString(){
        return "Le noeud '"+ this.getlabel() + "' a comme poids "+ this.getPoids() + " et comme enfants " + this.leftC.toString() + ";"+this.rightC.toString();
    }

    @Override
    public boolean egalLabel(String e) {
        return false;
    }

}
