public class NoeudFinal extends Noeud {
    private String label;
    private int poids;

    public NoeudFinal(String label, int poids){
        super();
        this.label=label;
        this.poids=poids;
    }

    @Override
    public String getlabel() {
        return label;
        
    }

    @Override
    public int getPoids() {
        return poids;
    }

    @Override
    public Noeud getLeftC() {
        return null;
    }

    @Override
    public Noeud getRightC() {
        return null;
    }

    public String toString(){
        return "Le noeud '"+ this.getlabel() + "' a comme poids "+ this.getPoids() ;
    }

    @Override
    public boolean egalLabel(String e) {
        return this.label.equals(e);
    }

}
