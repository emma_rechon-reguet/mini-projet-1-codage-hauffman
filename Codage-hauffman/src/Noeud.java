
public abstract class Noeud {
    public NoeudAutre parent;

    public Noeud(){
        super();
    }

    public abstract String getlabel();
    public abstract int getPoids();
    public abstract Noeud getLeftC();
    public abstract Noeud getRightC();
    public abstract boolean egalLabel(String e);
}
