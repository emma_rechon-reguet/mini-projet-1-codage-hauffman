import java.util.*;

public class Arbre {
    /* Ma liste de noeuds à traité. Va évoluer au fur et à mesure */
    private ArrayList<Noeud> noeuds;
    /* Le point de départ */
    private final LinkedHashMap <String, Integer> f;
    /* Mon arbre : un liste de noeuds */
    private ArrayList <Noeud> noeudArbre;
    /* La tete de ma liste  */
    private Noeud noeudTete;
    /* Création et enregistrement d'un poids max */
    private int pMax;
    
    public Arbre(String txt){
        Analyse_doc analyse_temp=new Analyse_doc(txt);
        analyse_temp.getAllOccurence();
        f=analyse_temp.trieOccurence();

        ArrayList<Noeud> noeudsRestants=new ArrayList<Noeud>();
        this.noeudArbre=new ArrayList<Noeud>();

        this.pMax=0;
        for (Map.Entry<String, Integer> entry : f.entrySet()){
            noeudsRestants.add(new NoeudFinal(entry.getKey() , entry.getValue()));
            this.noeudArbre.add(new NoeudFinal(entry.getKey() , entry.getValue()));
            pMax+=entry.getValue();
        }
        
        while (noeudsRestants.size()>1) {
            NoeudAutre n = new NoeudAutre(noeudsRestants.get(0),noeudsRestants.get(1));
            noeudsRestants.remove(0);
            noeudsRestants.remove(0);
            int poids=n.getPoids();
            int idx=0;
            while ((idx< noeudsRestants.size()) && (noeudsRestants.get(idx).getPoids()<poids)) {
                idx+=1;
            }
            noeudsRestants.add(idx, n);
            this.noeudArbre.add(idx,n);

        }
        this.noeudArbre.add(noeudsRestants.get(0));
        this.noeudTete=noeudsRestants.get(0);
    }

    public ArrayList<Noeud> getNoeuds(){
        return this.noeudArbre;
    }
    public Noeud getNoeudTete(){
        return this.noeudTete;
    }

    public int getPoidsMax(){
        return pMax;
    }

    public String toString(){
        String res="L'arbre contient : ";
        for (Noeud n : noeudArbre){
            res+="Le noeud "+n.getlabel()+" avec comme poids "+n.getPoids()+". ";
        }
        return res;
    }
}
