import java.util.ArrayList;
import java.util.Collections;

public class Encodage {
    private Arbre abr;
    private int[] enco;
    private ArrayList <int[]> encoCarac; 
    private ArrayList<Noeud> arbre;
    
    public Encodage(String txt){
        arbre=new ArrayList<Noeud> ();
        abr=new Arbre(txt);
        arbre=abr.getNoeuds();
        /* On inverse la liste afind d'avoir comme premier élément la liste*/
        Collections.reverse(arbre);
        /* Je prends la valeur maximum de profondeur comme largeur/taille de tableau */
        enco=new int [(getProfondeur(abr.getNoeudTete()))];
    }

    private int prof=0;
    public int getProfondeur(Noeud n){
        /* Méthode permettant de définir la profondir de l'arbre que l'on va créer */
        /* L'arbre est créé dans son getter + stocker dans la variable arbre */
        /* Je prends mon premier noeud de la liste (le noeud racine) en paramètre. NB : Ma liste est déjà inversée */
        /* Tant que mon fils gauche a un fils, je reprends cette méthode*/
        if (n == null) {
            return 0;
        } else {
            // Récursivement, calcule la profondeur des sous-arbres gauche et droit
            int profondeurGauche = getProfondeur(n.getLeftC());
            int profondeurDroit = getProfondeur(n.getRightC());

            // La profondeur de cet arbre est le maximum de la profondeur des sous-arbres gauche et droit, plus 1
            prof = Math.max(profondeurGauche, profondeurDroit) + 1;
        }
        return prof;
    }

    public Arbre getArbre(){
        return abr;
    }

    private boolean contient(String e){
        for(Noeud n : arbre){
            if(n.getlabel()==e){
                return true;
            }
        }
        return false;
    }

    private int trouverNoeud(String c){
        /* Classe permettant de définir si un noeud éxiste en fonction de sont label
         * Si le noeud n'existe pas : return null
         */
        idx=-1;
        int i=1;
        Noeud temp=arbre.get(0);
        while (temp.getlabel()!=null){
            if(temp.getlabel().equals(c)){
                return idx=arbre.indexOf(temp);
            }
            temp=arbre.get(i);
            i+=1;
        }
        return idx;
    }

    private int idx=0;
    public int [] getEncodageCarac(String cara){
        System.out.println(trouverNoeud(cara));
        if(trouverNoeud(cara)!=-1){
                /* Je pars du noeud correspondant au caractère */
                Noeud init=arbre.get(trouverNoeud(cara));
                /* Je prend son pere */
                Noeud tempo=init.parent;
                while(init.parent.parent!=null){
                    /* Je regarde si le noeud est l'enfant gauche (0) ou droit (1)  et j'ajoute la valeur */
                    if(tempo.getLeftC().equals(init)){
                        enco[idx]=0;
                        idx+=1;
                    }else if(tempo.getRightC().equals(init)){
                        enco[idx]=1;
                        idx+=1;
                    }
                    init=tempo.parent;
                    tempo=tempo.parent.parent;
                }
                
            }
        return enco;
    } 

    public int[] getEncodage(){
        /* Je parcours la liste de tous mes noeuds feuilles */

        /*  J'appelle la fonction qui fait l'encodage d'un caractère */

        /* J'ajoute le tableau obtenu dans mon tableau d'encodage */
        return enco;
    }
}
